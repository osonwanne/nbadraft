# How to Grab Request URL
# http://www.gregreda.com/2015/02/15/web-scraping-finding-the-api/
# Title:  Web Scraping 201: Finding the API 
# Date :  February 15, 2015

import requests

shots_url = 'http://stats.nba.com/stats/playerdashptshotlog?DateFrom=&DateTo=&GameSegment=&LastNGames=0&LeagueID=00&Location=&Month=0&OpponentTeamID=0&Outcome=&Period=0&PlayerID=202322&Season=2014-15&SeasonSegment=&SeasonType=Regular+Season&TeamID=0&VsConference=&VsDivision='


# request the URL and parse the JSON
response = requests.get(shots_url)
response.raise_for_status() 

print response.status_code

# raise exception if invalid response
shots = response.json()['resultSets'][0]['rowSet'] 

# do whatever we want with the shots data
# do_things(shots)