# How to Track NBA Player Movements in Python
# Tue 25 August 2015
# http://savvastjortjoglou.com/nba-play-by-play-movements.html

import requests
import pandas as pd
import numpy as np

# %matplotlib inline  #To make matplotlib inline by default in Jupyter (IPython 3)
import matplotlib.pyplot as plt
import seaborn as sns

from IPython.display import IFrame

sns.set_color_codes()
sns.set_style("white")

# IFrame('http://stats.nba.com/movement/#!/?GameID=0041400235&GameEventID=308', width=700, height=400)
